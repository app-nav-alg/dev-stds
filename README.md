# 仓库简介
本仓库包括应用导航算法项目的开发标准

#  文件编译
1. 可使用[TeXstudio](http://texstudio.sourceforge.net/)编辑本仓库的TeX文档；
2. 可使用[TeX Live](https://tug.org/texlive/)编译本仓库的TeX文档为PDF文档。